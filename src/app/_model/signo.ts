import { Paciente } from './paciente';
export class Signo {
    idSigno: number;
    pulso: string;
    ritmo: string;
    temperatura: string;
    fecha: string;
    paciente: Paciente;
}