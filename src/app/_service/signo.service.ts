import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { Signo } from './../_model/signo';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignoService  {
  
  signoCambio = new Subject<Signo[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST}/signos`;

  
  constructor(private http: HttpClient    ) {
  }

  listar() {
    return this.http.get<Signo[]>(this.url);
  }

  listarPorId(idSigno: number) {
    return this.http.get<Signo>(`${this.url}/${idSigno}`);
  }

  registrar(signo: Signo) {
    return this.http.post(this.url, signo);
  }

  modificar(signo: Signo) {
    return this.http.put(this.url, signo);
  }

  eliminar(idSigno: number) {
    return this.http.delete(`${this.url}/${idSigno}`);
  }

}
