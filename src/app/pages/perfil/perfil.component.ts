import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario: string;
  helper = new JwtHelperService();
  token: any;
  niveles:[];
  

  
  constructor(private formBuilder: FormBuilder) {
    this.token = sessionStorage.getItem(environment.TOKEN_NAME);
    const decodedToken = this.helper.decodeToken(this.token);
    this.usuario = decodedToken.user_name;
    //console.log(decodedToken.authorities);
    this.niveles = decodedToken.authorities;
  }

  ngOnInit() {  

    this.niveles.forEach(element => {
   //   console.log(element);
    });
  }

  
}
