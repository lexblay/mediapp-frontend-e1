import { SignoDialogComponent } from './signo-dialog/signo-dialog.component';
import { SignoModalComponent } from './../signo-modal/signo-modal.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { SignoService } from './../../../_service/signo.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Signo } from 'src/app/_model/signo';
import * as moment from 'moment';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;
  pacientes: Paciente[] = [];
  pacienteSeleccionado: Paciente;
  myControlPaciente: FormControl = new FormControl;
  pacientesFiltrados: Observable<any[]>;
  maxFecha: Date = new Date();
  fecha: Date = new Date();

  constructor(private route: ActivatedRoute,
    private signoService: SignoService,
    private router: Router,
    private dialog: MatDialog,
    private snack : MatSnackBar,
    private pacienteService: PacienteService) { }

  ngOnInit() {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'pulso': new FormControl(''),
      'fecha': new FormControl(''),
      'ritmo': new FormControl(''),
      'temperatura': new FormControl(''),
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;

      this.initForm();
      this.listarPaciente();
    });

    this.pacienteService.pacienteCambio.subscribe(data => {
      this.listarPaciente();
    });

    this.pacienteService.mensajeCambio.subscribe(data => {
      this.snack.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.pacientesFiltrados = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
  }

  filtrarPacientes(val: any) { 
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option => option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option => option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
    //return this.pacientes.filter(p => p.nombres.toLowerCase().includes(val));

  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  listarPaciente() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  initForm() {
    if (this.edicion) {
      this.signoService.listarPorId(this.id).subscribe(data => {

        
        this.myControlPaciente.setValue(data.paciente);

        
        
        
       
        
        
        
        this.form = new FormGroup({
          'id': new FormControl(data.idSigno),
          'paciente': this.myControlPaciente,
          'pulso': new FormControl(data.pulso),
          'fecha': new FormControl((new Date(moment(data.fecha).format('YYYY-MM-DDTHH:mm:ss'))).toISOString()),
          'ritmo': new FormControl(data.ritmo),
          'temperatura': new FormControl(data.temperatura),
        });
      });

    }
  }
  operar() {
    let signo = new Signo();
    signo.idSigno = this.form.value['id'];
    signo.pulso = this.form.value['pulso'];
    signo.ritmo = this.form.value['ritmo'];
    signo.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DD');
    signo.paciente = this.form.value['paciente'];
    signo.temperatura = this.form.value['temperatura'];

    if (this.edicion) {
      //Servicio de edicion
      //console.log(this.form.value['paciente']);
      this.signoService.modificar(signo).subscribe(() => {
        this.signoService.listar().subscribe(data => {
          this.signoService.signoCambio.next(data);
          this.signoService.mensajeCambio.next('SE MODIFICO EL REGISTRO');
        });
      });


    } else {
      //Servicio de insercion
      this.signoService.registrar(signo).subscribe(() => {
        this.signoService.listar().subscribe(data => {
          this.signoService.signoCambio.next(data);
          this.signoService.mensajeCambio.next('SE AGREGO UN REGISTRO');
        });
      });
    }

    this.router.navigate(['signo']);
  }


  abrirDialogo() {
    this.dialog.closeAll();
    this.dialog.open(SignoDialogComponent, {
      width: '500px',
      disableClose: true,
      hasBackdrop: true,
      position: {
        top: '50px'        
      }
    });
  }


}
