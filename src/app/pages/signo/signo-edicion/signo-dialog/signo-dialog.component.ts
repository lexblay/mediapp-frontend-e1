import { Paciente } from './../../../../_model/paciente';
import { MatDialogRef } from '@angular/material';
import { PacienteService } from './../../../../_service/paciente.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signo-dialog',
  templateUrl: './signo-dialog.component.html',
  styleUrls: ['./signo-dialog.component.css']
})
export class SignoDialogComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;

  constructor(
    private dialogRef: MatDialogRef<SignoDialogComponent>,
    private pacienteService: PacienteService) { }

  ngOnInit() {
    console.log("iniciando");

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'apellidos': new FormControl('', Validators.required),
      'dni': new FormControl(''),
      'telefono': new FormControl(''),
      'direccion': new FormControl('')
    });
  }

  get f() { return this.form.controls; }

  cancelar() {
    this.dialogRef.close();
  }

  operar() {
    let paciente = new Paciente();    
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.dni = this.form.value['dni'];
    paciente.telefono = this.form.value['telefono'];
    paciente.direccion = this.form.value['direccion'];

    this.pacienteService.registrar(paciente).subscribe(() => {
      this.pacienteService.listar().subscribe(data => {
        this.pacienteService.pacienteCambio.next(data);
        this.pacienteService.mensajeCambio.next('SE REGISTRO UN NUEVO PACIENTE');
      });
    });
    this.cancelar();
  }


}
