import { SignoModalComponent } from './signo-modal/signo-modal.component';
import { ActivatedRoute } from '@angular/router';
import { SignoService } from './../../_service/signo.service';
import { MatTableDataSource, MatDialog, MatSnackBar, MatPaginator, MatSort } from '@angular/material';
import { Signo } from './../../_model/signo';
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-signo',
  templateUrl: './signo.component.html',
  styleUrls: ['./signo.component.css']
})
export class SignoComponent implements OnInit {

  displayedColumns = ['idSigno', 'pulso', 'ritmo', 'temperatura', 'fecha','paciente', 'acciones'];
  dataSource: MatTableDataSource<Signo>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private signoService: SignoService,
     private dialog: MatDialog, 
     private snack: MatSnackBar,     
     public route: ActivatedRoute) { }


  
  ngOnInit() {    
    this.signoService.listar().subscribe(data => {  
     // console.log(data);
          
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.signoService.signoCambio.subscribe(data => {
      this.signoService.listar().subscribe(data => {      
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    });

    this.signoService.mensajeCambio.subscribe(data => {
      this.snack.open(data, 'AVISO', {
        duration: 2000
      });
    });


  }

  eliminar(idSigno: number) {

      this.signoService.eliminar(idSigno).subscribe(() => {
        this.signoService.listar().subscribe(data => {
          this.signoService.signoCambio.next(data);
          this.signoService.mensajeCambio.next('SE ELIMINO');

        });
      });
  }


}
